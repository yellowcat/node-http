const path = require('path');
const fs = require('fs');
const myHttp = require('./http');
const port = process.env.PORT || 3000;

function requestHandler(socket) {
    console.log('---------------');
    console.log(socket.temp.url);
    console.log('---------------');

    let file = 'default.html';
    if (socket.temp.url && socket.temp.url !== '/') {
        file = socket.temp.url;
    }

    const filePath = path.join('public', file);

    fs.stat(filePath, (err, stat) => {
        if (err) {
            socket.end('404: file not found');
            return;
        }

        if (stat.isDirectory() === false) {
            const readStream = fs.createReadStream(filePath);

            readStream.on('open', () => {
                readStream.pipe(socket);
            });

            readStream.on('error', () => {
                socket.end('Stream Error');
            });

            socket.on('close', () => {
                readStream.destroy();
            });
        }
   });
}

const myServer = myHttp.createServer(socket => requestHandler(socket));

myServer.listen(port, () => {
    console.log(`server listening on port ${port}`);
});

myServer.on('error', err => {
    throw err;
});
