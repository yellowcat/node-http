const net = require('net');
const separator = '\r\n\r\n';
const requestInfoRegexp = /([A-Z]+)\s([^\s]+)\s[A-Z]+\/([\d\.]+)/;

function parseHeaders(headerStr) {
    const arr = headerStr.replace(separator, '').split('\r\n');

    const [, method, url, protocolVersion] = arr.shift().match(requestInfoRegexp);

    const headers = arr.reduce((result, header) => {
        const matches = header.match(/^([^:]+):\s(.+)/);
        if (matches && matches[1] && matches[2]) {
            result[matches[1]] = matches[2];
        }
        return result;
    }, {});

    return {
        headers,
        method,
        url,
        protocolVersion
    };
}

function handleConnection(socket, cb) {
    let request = '';
    socket.setEncoding('utf-8');
    socket.on('data', (data) => {
        request += data;
        if (new RegExp(separator).test(request)) {
            socket.temp = parseHeaders(request);
            cb(socket);
        }
    });
}

function createServer(cb) {
    const server = net.createServer(socket => handleConnection(socket, cb));
    return server;
}

module.exports = {
    createServer
};
